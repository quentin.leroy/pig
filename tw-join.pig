-- TODO: load the input dataset, located in ./local-input/OSN/tw.txt
A = LOAD './tw.txt' AS (id: long, fr: long);
B = FOREACH A GENERATE *;


-- TODO: compute all the two-hop paths 
twohop = JOIN A BY $1, B BY $0;

-- TODO: project the twohop relation such that in output you display only the start and end nodes of the two hop path
p_result = FOREACH twohop GENERATE $0, $3;

-- TODO: make sure you avoid loops (e.g., if user 12 and 13 follow eachother) 
result = FILTER p_result BY ($0!=$1);

STORE result INTO './local-output/OSN/twj/';
