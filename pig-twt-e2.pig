dataset = LOAD './tw.txt' AS (id: long, fr: long);

-- TODO: check if user IDs are valid (e.g. not null) and clean the dataset
B = FILTER dataset BY (id>0);

-- TODO: organize data such that each node ID is associated to a list of neighbors
C = FOREACH B GENERATE id;

D = GROUP C BY id;

friends = FOREACH D GENERATE group, COUNT(C) AS count;

friends2 = FILTER friends BY (count > 2);

STORE friends2 INTO './twitter/results-e2';
