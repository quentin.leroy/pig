dataset = LOAD './tw.txt' AS (id: long, fr: long);

-- TODO: check if user IDs are valid (e.g. not null) and clean the dataset
B = FILTER dataset BY (id>0);

-- TODO: organize data such that each node ID is associated to a list of neighbors
C = FOREACH B GENERATE fr;

DUMP C;

D = GROUP C BY fr;

DUMP D;

friends = FOREACH D GENERATE group, COUNT(C);

STORE friends INTO './twitter/results-e1-3';
